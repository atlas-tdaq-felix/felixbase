#include <set>
#include <catch2/catch_test_macros.hpp>

#include "felixbase/common.hpp"

using namespace felix::base;

const size_t BUFFERSIZE = 32 * TOGBT_BLOCK_BYTES;
static char BIGBUF[BUFFERSIZE];


TEST_CASE( "The selected destination is too small to hold the data", "[toflx]" )
{
  REQUIRE( encode_data(0, BUFFERSIZE + 99, 0, BUFFERSIZE, 0) < 0 );
}

TEST_CASE( "Hello, World!", "[toflx]" )
{
  int len = encode_data((uint8_t *) "Hello, World!", 13, BIGBUF, BUFFERSIZE, 8);
  REQUIRE( len == TOGBT_BLOCK_BYTES );
  // Header #01
  REQUIRE( BIGBUF[--len] == 0x01 );
  REQUIRE( BIGBUF[--len] == 0x0f );
  // Message
  REQUIRE( BIGBUF[--len] ==  'H' );
  REQUIRE( BIGBUF[--len] ==  'e' );
  REQUIRE( BIGBUF[--len] ==  'l' );
  REQUIRE( BIGBUF[--len] ==  'l' );
  REQUIRE( BIGBUF[--len] ==  'o' );
  REQUIRE( BIGBUF[--len] ==  ',' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'W' );
  REQUIRE( BIGBUF[--len] ==  'o' );
  REQUIRE( BIGBUF[--len] ==  'r' );
  REQUIRE( BIGBUF[--len] ==  'l' );
  REQUIRE( BIGBUF[--len] ==  'd' );
  REQUIRE( BIGBUF[--len] ==  '!' );
  // Padding
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
}


TEST_CASE( "Salvador Dali", "[toflx]" )
{
#define QUOTE (uint8_t *) "Have no fear of perfection, you'll never reach it."
  int len = encode_data(QUOTE, 50, BIGBUF, BUFFERSIZE, 3);
  REQUIRE( len == 2 * TOGBT_BLOCK_BYTES );
  // Header #01
  REQUIRE( BIGBUF[--len] == 0x00 );
  REQUIRE( BIGBUF[--len] == 0x75 );
  // Message (to be continued ..)
  REQUIRE( BIGBUF[--len] ==  'u' );
  REQUIRE( BIGBUF[--len] == '\'' );
  REQUIRE( BIGBUF[--len] ==  'l' );
  REQUIRE( BIGBUF[--len] ==  'l' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'n' );
  REQUIRE( BIGBUF[--len] ==  'e' );
  REQUIRE( BIGBUF[--len] ==  'v' );
  REQUIRE( BIGBUF[--len] ==  'e' );
  REQUIRE( BIGBUF[--len] ==  'r' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'r' );
  REQUIRE( BIGBUF[--len] ==  'e' );
  REQUIRE( BIGBUF[--len] ==  'a' );
  REQUIRE( BIGBUF[--len] ==  'c' );
  REQUIRE( BIGBUF[--len] ==  'h' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'i' );
  REQUIRE( BIGBUF[--len] ==  't' );
  REQUIRE( BIGBUF[--len] ==  '.' );
  // Padding
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  REQUIRE( BIGBUF[--len] =='\xff' );
  // Header #02
  REQUIRE( BIGBUF[--len] == 0x00 );
  REQUIRE( BIGBUF[--len] == 0x7e );
  // Message
  REQUIRE( BIGBUF[--len] ==  'H' );
  REQUIRE( BIGBUF[--len] ==  'a' );
  REQUIRE( BIGBUF[--len] ==  'v' );
  REQUIRE( BIGBUF[--len] ==  'e' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'n' );
  REQUIRE( BIGBUF[--len] ==  'o' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'f' );
  REQUIRE( BIGBUF[--len] ==  'e' );
  REQUIRE( BIGBUF[--len] ==  'a' );
  REQUIRE( BIGBUF[--len] ==  'r' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'o' );
  REQUIRE( BIGBUF[--len] ==  'f' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'p' );
  REQUIRE( BIGBUF[--len] ==  'e' );
  REQUIRE( BIGBUF[--len] ==  'r' );
  REQUIRE( BIGBUF[--len] ==  'f' );
  REQUIRE( BIGBUF[--len] ==  'e' );
  REQUIRE( BIGBUF[--len] ==  'c' );
  REQUIRE( BIGBUF[--len] ==  't' );
  REQUIRE( BIGBUF[--len] ==  'i' );
  REQUIRE( BIGBUF[--len] ==  'o' );
  REQUIRE( BIGBUF[--len] ==  'n' );
  REQUIRE( BIGBUF[--len] ==  ',' );
  REQUIRE( BIGBUF[--len] ==  ' ' );
  REQUIRE( BIGBUF[--len] ==  'y' );
  REQUIRE( BIGBUF[--len] ==  'o' );
}


TEST_CASE( "ITK Message", "[toflx]" )
{
  const int SIZE = 349;
  uint8_t* data = new uint8_t[SIZE];
  memset(data, 0, SIZE);
  data[0] = 0xf0;
  data[1] = 0xf0;
  data[2] = 0x0f;
  data[3] = 0x0f;
  data[4] = 0x00;
  data[5] = 0xf0;

  int len = encode_data(data, SIZE, BIGBUF, BUFFERSIZE, 8);
  REQUIRE( len == 12*TOGBT_BLOCK_BYTES );

  REQUIRE( (uint8_t)BIGBUF[29] == 0xf0 );
  for(unsigned i=TOGBT_BLOCK_BYTES; i<11*TOGBT_BLOCK_BYTES; i++)
  {
    if(i % 32 == 30) {
      REQUIRE((uint8_t)BIGBUF[i] == 0x1e);
    } else if (i % 32 == 31) {
      REQUIRE((uint8_t)BIGBUF[i] == 0x01);
    } else {
      REQUIRE((uint8_t)BIGBUF[i] == 0);
    }
  }
}


// name: gen_random
// description: create a random alpha-numeric string
// \param  s    pointer to the destination where the content is to be written.
// \param  len  generate random string, with given length, len
// \return A random string is returned
char * gen_random(char *s, const int len)
{
  // init seed
  srand(time(NULL));
  static const char alphanum[] = \
    "0123456789"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz";
  for (int i = 0; i < len; ++i)
  {
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  }
  s[len] = '\0';
  return s;
}


TEST_CASE( "Boundary value test", "[toflx]" )
{
  { // 32 bytes ...
    char * string = gen_random(new char[TOGBT_PAYLOAD_BYTES + 1], TOGBT_PAYLOAD_BYTES);
    int len = encode_data((uint8_t*)string, TOGBT_PAYLOAD_BYTES, BIGBUF, BUFFERSIZE, 0);
    delete[] string;
    REQUIRE(len == TOGBT_BLOCK_BYTES);
  }
  { // 96 bytes ...
    char * string = gen_random(new char[3*TOGBT_PAYLOAD_BYTES + 1], 3*TOGBT_PAYLOAD_BYTES);
    int len = encode_data((uint8_t*)string, 3*TOGBT_PAYLOAD_BYTES, BIGBUF, BUFFERSIZE, 0);
    delete[] string;
    REQUIRE(len == 3*TOGBT_BLOCK_BYTES);
  }
  { // 257 bytes ...
    char * string = gen_random(new char[35*TOGBT_PAYLOAD_BYTES + 2], 35*TOGBT_PAYLOAD_BYTES + 1);
    int len = encode_data((uint8_t*)string, 35*TOGBT_PAYLOAD_BYTES + 1, BIGBUF, BUFFERSIZE, 0);
    delete[] string;
    REQUIRE(len < 0);
  }
}

TEST_CASE( "Test empty string", "[parse_range]" )
{
  std::set<unsigned int> elinks;
  parse_range(elinks, "");
  REQUIRE(elinks.empty());
}

TEST_CASE( "Test decimal range", "[parse_range]" )
{
  std::set<unsigned int> elinks;
  parse_range(elinks, "1,5,10-15,9");
  REQUIRE(elinks.size() == 9);
  REQUIRE(elinks.find(1) != elinks.end());
  REQUIRE(elinks.find(5) != elinks.end());
  REQUIRE(elinks.find(10) != elinks.end());
  REQUIRE(elinks.find(11) != elinks.end());
  REQUIRE(elinks.find(12) != elinks.end());
  REQUIRE(elinks.find(13) != elinks.end());
  REQUIRE(elinks.find(14) != elinks.end());
  REQUIRE(elinks.find(15) != elinks.end());
  REQUIRE(elinks.find(9) != elinks.end());
}

TEST_CASE( "Test hexadecimal range", "[parse_range]" )
{
  std::set<unsigned int> elinks;
  parse_range(elinks, "0xa-0xf");
  REQUIRE(elinks.size() == 6);
  REQUIRE(elinks.find(0xa) != elinks.end());
  REQUIRE(elinks.find(0xb) != elinks.end());
  REQUIRE(elinks.find(0xc) != elinks.end());
  REQUIRE(elinks.find(0xd) != elinks.end());
  REQUIRE(elinks.find(0xe) != elinks.end());
  REQUIRE(elinks.find(0xf) != elinks.end());
}

TEST_CASE( "Test weird-looking string", "[parse_range]" )
{
  std::set<unsigned int> elinks;
  parse_range(elinks, " ,,0xf - 10,  ,,,0xff,,255, Hello, World!");
  REQUIRE(elinks.size() == 7);
  REQUIRE(elinks.find(10) != elinks.end());
  REQUIRE(elinks.find(11) != elinks.end());
  REQUIRE(elinks.find(12) != elinks.end());
  REQUIRE(elinks.find(13) != elinks.end());
  REQUIRE(elinks.find(14) != elinks.end());
  REQUIRE(elinks.find(0xf) != elinks.end());
  REQUIRE(elinks.find(255) != elinks.end());
}

TEST_CASE( "Test vector with random numbers", "[parse_range]" )
{
  std::vector<int> diceRoller = populate_random(99, 1, 6);
  REQUIRE( diceRoller.size() == 99 );
  REQUIRE( *min_element(begin(diceRoller), end(diceRoller)) >= 1 );
  REQUIRE( *max_element(begin(diceRoller), end(diceRoller)) <= 6 );
  REQUIRE( (typeid(populate_random()) == typeid(std::vector<int>)) );
}

TEST_CASE( "IsOneAway?", "[parse_range]" )
{
  REQUIRE( isoneaway((char *)"Random", (char *)"random") == true );
  REQUIRE( isoneaway((char *)"rnd", (char *)"rand") == true );
  REQUIRE( isoneaway((char *)"RAndom", (char *)"random") == false );
}

// EOF
