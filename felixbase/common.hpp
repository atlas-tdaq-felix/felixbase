#ifndef FELIXBASE_COMMON_HPP
#define FELIXBASE_COMMON_HPP

#include <set>
#include <chrono>
#include <vector>
#include <string>
#include <random>
#include <functional>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <utility>

#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "felixbase/logging.hpp"

enum flx_fromhost_format {
  FROMHOST_FORMAT_REGMAP4,
  FROMHOST_FORMAT_5BIT_LENGTH,
  FROMHOST_FORMAT_HDR32_PACKET32,
  FROMHOST_FORMAT_HDR32_PACKET64,
  // Header with 8-bit fields (see FLX-2294):
  FROMHOST_FORMAT_HDR32_PACKET32_8B,
  FROMHOST_FORMAT_HDR32_PACKET64_8B,
  FROMHOST_FORMAT_HDR32_PACKET128_8B
};

// FromHost data block
#define TOFLX_EOM_MASK_RM4          0x0001
#define TOFLX_EOM_SHIFT_RM4         0
#define TOFLX_LENGTH_MASK_RM4       0x001E
#define TOFLX_LENGTH_SHIFT_RM4      1
#define TOFLX_ELINK_MASK_RM4        0xFFE0
#define TOFLX_ELINK_SHIFT_RM4       5
#define TOFLX_LENGTH_MASK           0x001F
#define TOFLX_ELINK_SHIFT           5
#define TOFLX_LENGTH_MASK_HDR32     0x003F
#define TOFLX_ELINK_SHIFT_HDR32     6
#define TOFLX_LENGTH_MASK_HDR32_8B  0x000000FF
#define TOFLX_ELINK_SHIFT_HDR32_8B  8
// Legacy (for the test cases)
#define TOGBT_BLOCK_BYTES           (16*2)
#define TOGBT_PAYLOAD_BYTES         (15*2)

namespace felix
{
namespace base
{
  template< typename T >
  std::string hex( T i )
  {
    std::stringstream stream;
    stream << "0x"
           << std::setfill ('0') << std::setw(sizeof(T)*2)
           << std::hex << i;
    return stream.str();
  }

  inline void dump_buffer(u_long vaddr, int size, u_long startAddress = 0, u_long virtualAddress = 0)
  {
    INFO("Block Addr:   " << hex(startAddress + vaddr - virtualAddress));
    u_char *buf = (u_char *)vaddr;
    int i;

    for(i = 0; i < size; i++)
    {
      if (i % 32 == 0) {
        printf("\n0x%016lx : ", i + startAddress + vaddr - virtualAddress);
      }
      printf("%02x ", *buf++);
    }
    printf("\n");
  }

  // name: encode_data
  // description: the following code encodes a message.
  // \param  source       data to be encoded.
  // \param  size         maximum number of bytes to be encoded from source.
  // \param  destination  pointer to the destination array where the content is
  //                      to be written.
  // \param  dest_size    size of destiantion.
  // \param  elink        elink number to be written to header.
  // \param  fromhost_data_format  type of FromHost data blocks to compile.
  // \return If successful, the total number of bytes of encoded data is returned.
  //         On failure, -1 is returned.
  inline int encode_data( uint8_t * source,
                          size_t size,
                          char * destination,
                          size_t dest_size,
                          uint64_t elink,
                          int fromhost_data_format = FROMHOST_FORMAT_REGMAP4 )
  {
    int header_size, block_bytes, payload_bytes, length_mask, elink_shift;

    if( fromhost_data_format == FROMHOST_FORMAT_REGMAP4 ) {
      header_size   = 2;
      block_bytes   = 32;
      payload_bytes = 30;
      length_mask   = TOFLX_LENGTH_MASK_RM4;
      elink_shift   = TOFLX_ELINK_SHIFT_RM4;
    }
    else if( fromhost_data_format == FROMHOST_FORMAT_HDR32_PACKET64 ) {
      header_size   = 4;
      block_bytes   = 64;
      payload_bytes = 60;
      length_mask   = TOFLX_LENGTH_MASK_HDR32;
      elink_shift   = TOFLX_ELINK_SHIFT_HDR32;
    }
    else if( fromhost_data_format == FROMHOST_FORMAT_HDR32_PACKET32 ) {
      header_size   = 4;
      block_bytes   = 32;
      payload_bytes = 28;
      length_mask   = TOFLX_LENGTH_MASK_HDR32;
      elink_shift   = TOFLX_ELINK_SHIFT_HDR32;
    }
    else if( fromhost_data_format == FROMHOST_FORMAT_HDR32_PACKET128_8B ||
             fromhost_data_format == FROMHOST_FORMAT_HDR32_PACKET64_8B ||
             fromhost_data_format == FROMHOST_FORMAT_HDR32_PACKET32_8B ) {
      // Keeping the existing 4-byte header e-link numbering,
      // so map the e-link number to the '8-bit-fields' version
      uint64_t linknr = (elink & 0x07C0) >> 6;
      uint64_t eindex = (elink & 0x003F);
      elink = (linknr << 8) | eindex;

      header_size = 4;
      if( fromhost_data_format == FROMHOST_FORMAT_HDR32_PACKET128_8B )
        block_bytes = 128;
      else if( fromhost_data_format == FROMHOST_FORMAT_HDR32_PACKET64_8B )
        block_bytes = 64;
      else
        block_bytes = 32;
      payload_bytes = block_bytes - 4;
      length_mask   = TOFLX_LENGTH_MASK_HDR32_8B;
      elink_shift   = TOFLX_ELINK_SHIFT_HDR32_8B;
    }
    else if( fromhost_data_format == FROMHOST_FORMAT_5BIT_LENGTH ) {
      header_size   = 2;
      block_bytes   = 32;
      payload_bytes = 30;
      length_mask   = TOFLX_LENGTH_MASK;
      elink_shift   = TOFLX_ELINK_SHIFT;
    }
    else {
      // Assume FROMHOST_FORMAT_REGMAP4
      header_size   = 2;
      block_bytes   = 32;
      payload_bytes = 30;
      length_mask   = TOFLX_LENGTH_MASK_RM4;
      elink_shift   = TOFLX_ELINK_SHIFT_RM4;
    }

    int blocks, hdr, final_hdr, final_bytes;

    blocks = (size + payload_bytes-1) / payload_bytes;
    final_bytes = size - (blocks-1) * payload_bytes;
    if( fromhost_data_format != FROMHOST_FORMAT_REGMAP4 )
      {
        // Regmap 0x0500 and beyond, see FLX-1355, FLX-1601 and FLX-1886
        hdr = ((elink << elink_shift) | length_mask);
        final_hdr  = hdr & ~length_mask;
        final_hdr |= final_bytes; // Length in bytes
      }
    else
      {
        // Regmap 0x0400
        hdr = ((elink << elink_shift) | ((payload_bytes/2) << TOFLX_LENGTH_SHIFT_RM4));
        int final_size = (final_bytes + 1) / 2; // Length in 2-byte units
        final_hdr  = hdr & ~length_mask;
        final_hdr |= ((final_size << TOFLX_LENGTH_SHIFT_RM4) | TOFLX_EOM_MASK_RM4);
      }

    // Just in case (and to satisfy the test cases in folder test)
    if( (size_t) blocks*block_bytes > dest_size )
      return -1;

    int    src_i  = 0;
    size_t dest_i = 0;

    // Fully-filled FromHost data packets
    for (int i = 0; i < blocks-1; ++i) {
      // Payload byte order MSB to LSB
      for(int j = payload_bytes-1; j >= 0; --j, ++dest_i) {
        destination[dest_i] = source[src_i + j] & 0xFF;
      }
      src_i += payload_bytes;

      // Header
      for (int j=0; j<header_size; ++j, ++dest_i)
        destination[dest_i] = (char) ((hdr >> j*8) & 0xFF);

      // Wrap-around ?
      // (NB: check not needed due to 'dest_size' check before this for-loop)
      //if( dest_i >= dest_size )
      //  dest_i = 0;
    }

    // Final (likely not fully filled) FromHost data packet

    // Payload byte order MSB to LSB
    for (int j = payload_bytes-1; j >= final_bytes; --j, ++dest_i) {
      // NB: test case expects 0xFF..
      destination[dest_i] = (char) 0xFF; // Remaining payload bytes: set to 0xFF
      //destination[dest_i] = (char) 0x00; // Remaining payload bytes: set to 0x00
    }
    for (int j = final_bytes-1; j >= 0; --j, ++dest_i) {
      destination[dest_i] = source[src_i + j] & 0xFF; // Final data bytes
    }

    // Header
    for (int j=0; j<header_size; ++j, ++dest_i)
      destination[dest_i] = (char) ((final_hdr >> j*8) & 0xFF);

    // Return the size of the encoded message
    size_t encoded_size = blocks * block_bytes;
    return encoded_size;
  }

  // name : parse_range
  // description : Algorithm to extract numbers/number range from a string.
  // \param  #posIntSet  reference to the destination set where the content is to be stored.
  // \param   line  string to be transformed. Ex: 1,5,24-31,34-35,38-39,65,69,88-95,98,99,102-103
  // \return posIntSet is returned.
  inline std::set<unsigned int> parse_range(std::set<unsigned int>& posIntSet, std::string line)
  {
    // Algorithm used:
    //  * Split string using ,
    //  * Split the individual string using -
    //  * Make a range low and high
    //  * Insert it into set with help of this range

    // remove spaces from line
    line.erase( std::remove(line.begin(), line.end(),' '), line.end() );

    std::vector<std::string> strs, r;
    int low, high, base;

    // split string by comma
    boost::split(strs, line, boost::is_any_of(","));

    // Find if a given string conforms to hex notation.
    auto isHexNotation = [](const std::string& s) {
      // Check that the first portion of the string is the literal "0x" and that the remainder of
      // the string contains only the allowed characters.
      return s.compare(0, 2, "0x") == 0
        && s.size() > 2
        && s.find_first_not_of("0123456789abcdefABCDEF", 2) == std::string::npos;
    };

    for (auto it : strs)
    {
      if (it == "" || it[strspn(it.c_str(), "-0x123456789abcdefABCDEF")])
      {
        // empty string or illegal characters found
        continue;
      }

      // split string by dash
      boost::split(r, it, boost::is_any_of("-"));

      base = isHexNotation(r[0]) ? 16 : 10;
      low = high = boost::lexical_cast<int> ( strtol(r[0].c_str(), NULL, base) );

      if(++r.begin() != r.end())
      {
        base = isHexNotation(r[1]) ? 16 : 10;
        high = boost::lexical_cast<int> ( strtol(r[1].c_str(), NULL, base) );
      }

      // high is low and low is high..
      if(low > high)
      {
        high^= low;
        low ^= high;
        high^= low;
      }

      for(int i = low;i <= high; ++i)
      {
        posIntSet.insert(i);
      }
    }

    return posIntSet;
  }

  inline bool oneeditaway(char* s1, char* s2)
  {
    int i, len = strlen(s1);
    bool flag = false;
    for (i = 0; i < len; i++)
    {
      if (s1[i] != s2[i])
      {
        if (flag)
        {
          return false;
        }
        flag = true;
      }
    }
    return true;
  }

  inline bool oneinsertaway(char* s1, char* s2)
  {
    int i1 = 0, i2 = 0, len = strlen(s2);
    while(i2 < len)
      for(i1 = i2 = 0; i1 < len; )
      {
        if(s1[i1] != s2[i2])
        {
          if(i1 != i2)
          {
            return false;
          }
          i2++;
        }
        else
        {
          i1++;
          i2++;
        }
      }
    return true;
  }

  // name : isoneaway
  // description : There are three types of edits that can be performed on strings: insert a
  //               character, remove a character and replace a character.
  // \param  s1  C string to be compared.
  // \param  s2  C string to be compared.
  // \return true if one away, otherwise false.
  inline bool isoneaway(char* s1, char* s2)
  {
    if(strlen(s1) == strlen(s2))
    {
      return oneeditaway(s1, s2);
    }
    else if(strlen(s1) == strlen(s2) - 1)
    {
      return oneinsertaway(s1, s2);
    }
    else if(strlen(s2) == strlen(s1) - 1)
    {
      return oneinsertaway(s2, s1);
    }

    return false;
  }

  // name : populate_random
  // description : Algorithm to fill a vector of n elements with random numbers.
  //               example: a_dist=1 and b_dist=6 -> simulates throwing 6-sided dice.
  // \param  n       # of random numbers.
  // \param  a_dist  an integer value for the lower bound of the distribution.
  // \param  b_dist  an integer value for the upper bound of the distribution.
  // \return vector with random numbers.
  inline std::vector<int> populate_random(int n = 10, int a_dist = 0, int b_dist = 100)
  {
    // First create an instance of an engine.
    std::random_device rnd_device;

    // Specify the engine and distribution.
    std::mt19937 mersenne_engine(rnd_device());
    std::uniform_int_distribution<int> dist(a_dist, b_dist);

    auto gen = std::bind(dist, mersenne_engine);
    std::vector<int> vec(n);

    std::generate(begin(vec), end(vec), gen);
    return vec;
  }

  class Timer
  {
  public:
    Timer()
      : beg_(clock_::now()) {}

    void reset() {
      beg_ = clock_::now();
    }

    double elapsed() const {
      return std::chrono::duration_cast<second_>(clock_::now() - beg_).count();
    }

  private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
  };

}
}

#endif
