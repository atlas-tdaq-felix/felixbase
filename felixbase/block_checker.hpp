#ifndef FELIXBASE_BLOCK_CHECKER_HPP
#define FELIXBASE_BLOCK_CHECKER_HPP

#include <set>

#include "packetformat/block_format.hpp"

#include "felixbase/common.hpp"

namespace felix
{
namespace base
{

typedef felix::packetformat::block Block;

class BlockChecker {
  public:
    BlockChecker(unsigned int minElinkId = 0, unsigned int maxElinkId = 255)
    : minElinkId(minElinkId) {
      unsigned int range = 1 + maxElinkId - minElinkId;

      seqnr = std::vector<unsigned>(range, 0);
    }

    ~BlockChecker() {
    }

    bool checkSignature(const Block* block) {
      return( block->sob == felix::packetformat::START_OF_BLOCK ||
              block->sob == felix::packetformat::START_OF_BLOCK_32B ||
              (block->sob & 0xC0FF) == felix::packetformat::START_OF_BLOCK_PHASE2 );
    }

    int checkSeqNo(const Block* block) {
      unsigned expected = seqnr[block->elink - minElinkId];
      seqnr[block->elink - minElinkId] = (block->seqnr + 1) % 32;
      return expected == block->seqnr ? -1 : expected;
    }

    bool checkSubchunks(const Block* block) {
      // FIXME to be implemented
      return true;
    }

    bool check(const Block *block) {
      if (!checkSignature(block)) {
        ERROR("No Valid Signature found in block");
        return false;
      }

      int expected = checkSeqNo(block);
      if (expected >= 0) {
        ERROR("Wrong Sequence Number found in block, found " << block->seqnr <<
             " instead of  " << expected <<
             " for elink " << hex(block->elink));
        return false;
      }

      if (!checkSubchunks(block)) {
        ERROR("Error in Subchunks");
        return false;
      }

      return true;
    }

  private:
    unsigned int minElinkId;
    std::vector<unsigned> seqnr;
};

}
}

#endif
